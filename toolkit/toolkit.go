package toolkit

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	ose "os/exec"
	"reflect"
	"runtime"
	"strings"
)

type GOOS string

func (g GOOS) String() string {
	return string(g)
}

const (
	Windows GOOS = "windows"
	Linux   GOOS = "linux"
	Darwin  GOOS = "darwin"
	FreeBSD GOOS = "freebsd"
)

// GetGOOS 获取系统信息
func GetGOOS() GOOS {
	if runtime.GOOS == "windows" {
		return Windows
	}
	if runtime.GOOS == "darwin" {
		return Darwin
	}
	if runtime.GOOS == "freebsd" {
		return FreeBSD
	}
	return Linux
}

// GetGOARCH 获取架构信息
func GetGOARCH() string {
	return runtime.GOARCH
}

// GetGOBIN 获取go相关的二进制目录 没有 GOBIN 则找 GOROOT
func GetGOBIN() string {
	out, err := ose.Command("go", "env", "GOBIN").CombinedOutput()
	if err != nil {
		if GetGOOS() == Windows {
			return fmt.Sprintf("%s\\bin", runtime.GOROOT())
		}
		return fmt.Sprintf("%s/bin", runtime.GOROOT())
	}
	outP := strings.Trim(string(out), " \n")
	if outP == "" {
		if GetGOOS() == Windows {
			return fmt.Sprintf("%s\\bin", runtime.GOROOT())
		}
		return fmt.Sprintf("%s/bin", runtime.GOROOT())
	}
	return outP
}

func GetDirectorySeparator() string {
	if GetGOOS() == Windows {
		return "\\"
	}

	return "/"
}

// 校正文件路径分隔符
func CorrectingDirSeparator(filepath string) string {
	separator := GetDirectorySeparator()
	if GetGOOS() == Windows {
		return strings.ReplaceAll(filepath, "/", separator)
	}
	return strings.ReplaceAll(filepath, "\\", separator)
}

func IsCurrentDirHasModfile() bool {
	modFile := "go.mod"
	exist := IsExist(modFile)

	if !exist {
		_, _ = fmt.Fprintf(os.Stderr, "go.mod not found")
	}

	return exist
}

func GetCurrentModuleName() (modName string, err error) {
	modName = ""

	text, err := GetFileLineOne("go.mod")
	if err != nil {
		return
	}

	if len(text) < 8 {
		err = errors.New("go.mod incorrect file format！")
		return
	}

	modName = text[7 : len(text)-1]
	modName = strings.Trim(modName, "\r")
	modName = strings.Trim(modName, "\n")

	return
}

func IsExist(path string) bool {
	_, err := os.Stat(path)
	if err != nil {
		if os.IsExist(err) {
			return true
		}
		if os.IsNotExist(err) {
			return false
		}
		return false
	}
	return true
}

func GetFileLineOne(filePath string) (lineOneText string, err error) {
	lineOneText = ""
	f, err := os.Open(filePath)
	defer f.Close()
	if err != nil {
		err = errors.New(filePath + " open file error: " + err.Error())
		return
	}
	//建立缓冲区，把文件内容放到缓冲区中
	buf := bufio.NewReader(f)
	//遇到\n结束读取
	b, err := buf.ReadBytes('\n')
	if err != nil {
		if err == io.EOF {
			err = errors.New(filePath + " is empty! ")
			return
		}
		err = errors.New(filePath + " read bytes error: " + err.Error())
		return
	}
	lineOneText = string(b)
	err = nil
	return
}

// BaseTypeInArray 基本数据类型的判断是否在数组内，是则返回true以及下标
func BaseTypeInArray(val interface{}, array interface{}) (exists bool, index int) {
	exists = false
	index = -1
	switch reflect.TypeOf(array).Kind() {
	case reflect.Slice:
		s := reflect.ValueOf(array)
		length := s.Len()
		for i := 0; i < length; i++ {
			if reflect.DeepEqual(val, s.Index(i).Interface()) == true {
				index = i
				exists = true
				return
			}
		}
	}
	return
}

func FileNameCheck(fileName string) error {
	str := "abcdefghijklmnopqrstuvwxyz_/"

	sli := strings.Split(str, "")
	for _, v := range strings.Split(fileName, "") {
		if exist, _ := BaseTypeInArray(v, sli); !exist {
			return errors.New("file name only contain a-z || _ || / ")
		}
	}

	if fileName[:1] == "/" {
		return errors.New("full path first word cannot be a directory separator")
	}
	if fileName[len(fileName)-1:] == "/" {
		return errors.New("full path last word cannot be a directory separator")
	}

	fileNameSli := strings.Split(fileName, "/")

	for _, v1 := range fileNameSli {
		if v1[:1] == "_" {
			return errors.New("each path first word cannot be a underline")
		}
		if v1[len(v1)-1:] == "_" {
			return errors.New("each path last word cannot be a underline")
		}
	}

	return nil
}

func ActionNameCheck(name string) error {
	str := "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

	sli := strings.Split(str, "")
	for _, v := range strings.Split(name, "") {
		if exist, _ := BaseTypeInArray(v, sli); !exist {
			return errors.New("action name only contain a-z || A-Z ")
		}
	}

	if name[0] >= 'A' && name[0] <= 'Z' {
		return nil
	} else {
		return errors.New("action name first must be in A-Z")
	}
}

func CmdNameCheck(fileName string) error {
	str := "abcdefghijklmnopqrstuvwxyz_"

	sli := strings.Split(str, "")
	for _, v := range strings.Split(fileName, "") {
		if exist, _ := BaseTypeInArray(v, sli); !exist {
			return errors.New("cmd name only contain a-z || _")
		}
	}

	if fileName[:1] == "_" {
		return errors.New("cmd name first word cannot be a underline")
	}
	if fileName[len(fileName)-1:] == "_" {
		return errors.New("cmd name last word cannot be a underline")
	}

	return nil
}

// FirstUpper 字符串首字母大写
func FirstUpper(s string) string {
	if s == "" {
		return ""
	}
	return strings.ToUpper(s[:1]) + s[1:]
}

// FirstLower 字符串首字母小写
func FirstLower(s string) string {
	if s == "" {
		return ""
	}
	return strings.ToLower(s[:1]) + s[1:]
}

func ReadAll(filePth string) ([]byte, error) {
	f, err := os.Open(filePth)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	return ioutil.ReadAll(f)
}

func RewriteFile(path, str string) error {
	// 打开一个存在的文件，将原来的内容覆盖掉
	// O_WRONLY: 只写, O_TRUNC: 清空文件
	file, err := os.OpenFile(path, os.O_WRONLY|os.O_TRUNC, 0666)
	if err != nil {
		return err
	}

	defer file.Close() // 关闭文件
	// 带缓冲区的*Writer
	writer := bufio.NewWriter(file)
	_, err = writer.WriteString(str)
	if err != nil {
		return err
	}

	// 将缓冲区中的内容写入到文件里
	err = writer.Flush()
	if err != nil {
		return err
	}

	return nil
}

func ExecCommand(name string, arg ...string) error {
	_, err := ose.Command(name, arg...).CombinedOutput()
	if err != nil {
		return err
	}

	return nil
}

// CamelCaseToSnakeCase 驼峰转蛇形
func CamelCaseToSnakeCase(s string) string {
	data := make([]byte, 0, len(s)*2)
	j := false
	num := len(s)
	for i := 0; i < num; i++ {
		d := s[i]
		// or通过ASCII码进行大小写的转化
		// 65-90（A-Z），97-122（a-z）
		//判断如果字母为大写的A-Z就在前面拼接一个_
		if i > 0 && d >= 'A' && d <= 'Z' && j {
			data = append(data, '_')
		}
		if d != '_' {
			j = true
		}
		data = append(data, d)
	}
	//ToLower把大写字母统一转小写
	return strings.ToLower(string(data[:]))
}

// SnakeCaseToCamelCase 蛇形转驼峰
func SnakeCaseToCamelCase(s string) string {
	data := make([]byte, 0, len(s))
	j := false
	k := false
	num := len(s) - 1
	for i := 0; i <= num; i++ {
		d := s[i]
		if k == false && d >= 'A' && d <= 'Z' {
			k = true
		}
		if d >= 'a' && d <= 'z' && (j || k == false) {
			d = d - 32
			j = false
			k = true
		}
		if k && d == '_' && num > i && s[i+1] >= 'a' && s[i+1] <= 'z' {
			j = true
			continue
		}
		data = append(data, d)
	}
	return string(data[:])
}

func GetRequestOrResponseFilePath(fileName, dir string) string {
	sep := GetDirectorySeparator()

	fileName = strings.ReplaceAll(fileName, "/", sep)
	filePath := "internal" +
		sep + dir +
		sep + fileName + fmt.Sprintf("_%s.go", dir)

	return filePath
}

// FileForEach 只读取当前指定路径下的文件名，跳过文件夹
func FileForEach(fileFullPath string) (fileList []string, err error) {
	files, err := ioutil.ReadDir(fileFullPath)
	if err != nil {
		return
	}

	sep := GetDirectorySeparator()

	for _, file := range files {
		if file.IsDir() {
			continue
		}
		fileList = append(fileList, fileFullPath+sep+file.Name())
	}
	return
}
