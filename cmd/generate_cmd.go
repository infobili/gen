package cmd

import (
	"fmt"
	"gitee.com/infobili/gen/template_files"
	"gitee.com/infobili/gen/toolkit"
	"github.com/spf13/cobra"
	"io/fs"
	"io/ioutil"
	"strings"
)

var cmdName string

func init() {
	generateCMDCommand.PersistentFlags().StringVar(&cmdName, "name", "",
		"cmd name, underline split, like these： test_login || test ")
	rootCmd.AddCommand(generateCMDCommand)
}

var generateCMDCommand = &cobra.Command{
	Use:   "cmd",
	Short: "gen cmd",
	Run: func(cmd *cobra.Command, args []string) {

		// cmd name 校验
		cmdName = strings.ReplaceAll(cmdName, "_cmd", "")
		cmdName = strings.ReplaceAll(cmdName, "cmd", "")

		if cmdName == "" {
			fmt.Println("cmd name is required")
			return
		}

		if err := toolkit.CmdNameCheck(cmdName); err != nil {
			fmt.Printf("cmd name is illegal, %+v", err.Error())
			return
		}
		// cmd name校验

		cmdVar := toolkit.FirstLower(toolkit.SnakeCaseToCamelCase(cmdName)) + "Command"

		dir := "cmd"
		cmdFileList, err := toolkit.FileForEach(dir)
		if err != nil {
			fmt.Printf("gen cmd failed! get files under folder: cmd failed, err: %+v", err)
			return
		}

		for _, f := range cmdFileList {
			b, _ := toolkit.ReadAll(f)
			rawStr := string(b)
			// 有没有相同变量名
			if strings.Contains(rawStr, cmdVar) {
				fmt.Printf("gen cmd failed! %s had same variable name: %s", f, cmdVar)
				return
			}
			// 有没有相同的命令名
			if strings.Contains(rawStr, fmt.Sprintf("Use:   \"%s\"", cmdName)) {
				fmt.Printf("gen cmd failed! %s had same cmd name: %s", f, cmdVar)
				return
			}
		}

		sep := toolkit.GetDirectorySeparator()
		filePath := dir + sep + cmdName + "_cmd.go"

		content := fmt.Sprintf(template_files.CmdTemplate, cmdVar, cmdVar, cmdName, cmdName, cmdName, cmdName)

		err = ioutil.WriteFile(filePath, []byte(content), fs.ModePerm)
		if err != nil {
			fmt.Printf("gen cmd failed! failed to write %s: %+v\n", filePath, err)
			return
		}

		err = toolkit.ExecCommand("gofmt", "-w", filePath)
		if err != nil {
			fmt.Printf("cmd file: %s format failed", filePath)
			return
		}

		fmt.Printf("gen cmd success! please see the file: %s. if you want init some driver, you can see the init_driver_demo.go\n", filePath)
	},
}
