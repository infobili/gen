package cmd

import (
	"fmt"
	"gitee.com/infobili/gen/generate"
	"gitee.com/infobili/gen/template_files"
	"gitee.com/infobili/gen/toolkit"
	"github.com/spf13/cobra"
	"go/ast"
	"go/parser"
	"go/token"
	"io/fs"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

var moduleName string
var actionName string
var disableRequest bool
var disableResponse bool

func init() {
	generateControllerFuncCommand.PersistentFlags().StringVar(&moduleName, "module", "",
		"module file name, "+
			"underline split, like these： test_login || test || dir/test_login || dir/test, "+
			"generate controller file in the ./internal/controller, "+
			"generate request file in the ./internal/request "+
			"and generate response file in ./internal/response directory")
	generateControllerFuncCommand.PersistentFlags().StringVar(&actionName, "name", "",
		"action name, large hump, like these: Login || LoginAdmin")
	generateControllerFuncCommand.PersistentFlags().BoolVar(&disableRequest, "disable-request", false,
		"if the value is true, will not be generated request")
	generateControllerFuncCommand.PersistentFlags().BoolVar(&disableResponse, "disable-response", false,
		"if the value is true, will not be generated response")
	rootCmd.AddCommand(generateControllerFuncCommand)
}

var (
	generateControllerFuncCommand = &cobra.Command{
		Use:   "action",
		Short: "gen controller action",
		Run: func(cmd *cobra.Command, args []string) {
			var err error

			// controller name 校验
			if moduleName == "" {
				fmt.Println("module file name is required")
				return
			}

			if err := toolkit.FileNameCheck(moduleName); err != nil {
				fmt.Printf("module file name is illegal, %+v", err.Error())
				return
			}

			// action name校验
			if actionName == "" {
				fmt.Println("action name is required")
				return
			}
			if err := toolkit.ActionNameCheck(actionName); err != nil {
				fmt.Printf("action name is illegal, %+v", err.Error())
				return
			}

			sep := toolkit.GetDirectorySeparator() // 不同操作系统的文件夹分隔符

			// go.mod 校验
			modName := ""
			exist := toolkit.IsCurrentDirHasModfile() // 当前目录是否有mod文件
			if exist {
				var err error
				modName, err = toolkit.GetCurrentModuleName()
				if err != nil {
					fmt.Println(err.Error())
					return
				}
			} else {
				fmt.Println("your project missing go.mod")
				return
			}

			// request 生成逻辑
			var requestPkgName string
			var reqStruct string
			var reqAlias string
			if !disableRequest {
				requestPkgName, reqStruct, reqAlias = reqOrRespGenerate("request", moduleName, actionName)
			}

			// response 生成逻辑
			var responsePkgName string
			var respStruct string
			var respAlias string
			if !disableResponse {
				responsePkgName, respStruct, respAlias = reqOrRespGenerate("response", moduleName, actionName)
			}

			moduleName = strings.ReplaceAll(moduleName, "/", sep)
			filePath := "internal" +
				sep + "controller" +
				sep + moduleName + "_controller.go"

			var outputResp = "api.VoidStruct"

			controllerDecorateFilePath := strings.ReplaceAll(filePath, ".go", "_decorate.go")

			fileExist := toolkit.IsExist(filePath)
			// 控制器文件已存在
			if fileExist {

				b, _ := toolkit.ReadAll(filePath)
				rawStr := string(b)

				file, err := parser.ParseFile(token.NewFileSet(), "", rawStr, parser.ParseComments)

				if err != nil {
					fmt.Printf("parser controller: %s failed! error: %s", filePath, err.Error())
					return
				}

				decls := file.Decls

				for _, dV := range decls {
					dr, ok := dV.(*ast.FuncDecl)
					if ok && actionName == dr.Name.Name {
						// 控制器里的方法已存在
						fmt.Printf("controller file: %s already included function: %s", filePath, actionName)
						return
					}
				}

				functionContent := ""

				funcTmeplate := `func %s(c *gin.Context%s) (int32, interface{}) {
				// TODO Writing logical code%s

				return api.Success, %s
			}`
				if reqStruct != "" {
					reqStruct = fmt.Sprintf(",req *%s", reqStruct)
				}

				if respStruct != "" {
					respStruct = fmt.Sprintf(`

resp := new(%s)`, respStruct)
					outputResp = "resp"
				}
				functionContent = fmt.Sprintf(funcTmeplate, actionName, reqStruct, respStruct, outputResp)

				rawStr += "\n\n" + functionContent

				importSuffix := `import (
			`
				if reqStruct != "" {
					if !strings.Contains(rawStr, modName+"/"+requestPkgName) {
						rawStr = strings.ReplaceAll(rawStr, importSuffix, importSuffix+"\t"+reqAlias+"\""+modName+"/"+requestPkgName+"\""+"\n")
					}
				}

				if respStruct != "" {
					if !strings.Contains(rawStr, modName+"/"+responsePkgName) {
						rawStr = strings.ReplaceAll(rawStr, importSuffix, importSuffix+"\t"+respAlias+"\""+modName+"/"+responsePkgName+"\""+"\n")
					}
				}

				err = toolkit.RewriteFile(filePath, rawStr)
				if err != nil {
					fmt.Printf("controller file: %s add function:  failed! error: "+err.Error(), filePath, actionName)
					return
				}
				fmt.Printf("controller file: %s add function: %s success!\n", filePath, actionName)

				if reqStruct != "" {
					// 有请求参数的生成 controller decorate
					pkgName := "controller"
					if strings.Contains(strings.ReplaceAll(moduleName, sep, "/"), "/") {
						tmpSlic := strings.Split(strings.ReplaceAll(moduleName, sep, "/"), "/")
						pkgName = tmpSlic[len(tmpSlic)-2]
					}
					importReqContent := reqAlias + "\"" + modName + "/" + requestPkgName + "\"\n"

					generateControllerDecorate(controllerDecorateFilePath, pkgName, importReqContent, actionName, reqStruct)
				}
			} else {
				basePath := filepath.Dir(filePath)
				basePathExist := toolkit.IsExist(basePath)
				if !basePathExist {
					err := os.MkdirAll(basePath, os.ModePerm)
					if err != nil {
						fmt.Printf("create folder: %s recursively failed! error: "+err.Error(), basePath)
						return
					}
				}

				pkgName := "controller"
				errorCodePrefix := moduleName
				if strings.Contains(strings.ReplaceAll(moduleName, sep, "/"), "/") {
					tmpSlic := strings.Split(strings.ReplaceAll(moduleName, sep, "/"), "/")
					pkgName = tmpSlic[len(tmpSlic)-2]
					errorCodePrefix = tmpSlic[len(tmpSlic)-1]
				}

				importReqContent := ""
				if reqStruct != "" {
					importReqContent = reqAlias + "\"" + modName + "/" + requestPkgName + "\"\n"
					reqStruct = fmt.Sprintf(",req *%s", reqStruct)
				}

				importRespContent := ""
				if respStruct != "" {
					importRespContent = respAlias + "\"" + modName + "/" + responsePkgName + "\"" + "\n"
					respStruct = fmt.Sprintf(`

resp := new(%s)`, respStruct)
					outputResp = "resp"
				}

				content := fmt.Sprintf(template_files.ControllerTemplate, pkgName, importReqContent, importRespContent, toolkit.FirstLower(toolkit.SnakeCaseToCamelCase(errorCodePrefix)), toolkit.FirstLower(toolkit.SnakeCaseToCamelCase(errorCodePrefix)), actionName, reqStruct, respStruct, outputResp)

				err = ioutil.WriteFile(filePath, []byte(content), fs.ModePerm)
				if err != nil {
					fmt.Printf("failed to write %s: %+v\n", filePath, err)
					return
				}

				if reqStruct != "" {
					// 有请求参数的生成 controller decorate
					generateControllerDecorate(controllerDecorateFilePath, pkgName, importReqContent, actionName, reqStruct)
				}

			}
			err = toolkit.ExecCommand("gofmt", "-w", filePath)
			if err != nil {
				fmt.Printf("controller file: %s format failed", filePath)
				return
			}

			err = toolkit.ExecCommand("gofmt", "-w", controllerDecorateFilePath)
			if err != nil {
				fmt.Printf("controller decorate file: %s format failed", controllerDecorateFilePath)
				return
			}

			fmt.Printf("generate controller function success! please see the file: %s\n", filePath)
		},
	}
)

func generateControllerDecorate(filePath, pkgName, importReqContent, actionName, reqStruct string) {
	fileExist := toolkit.IsExist(filePath)
	reqStruct = strings.ReplaceAll(reqStruct, ",req ", "")
	if !fileExist {
		content := fmt.Sprintf(template_files.ControllerDecorateTemplate, pkgName, pkgName, importReqContent, actionName, actionName, reqStruct)

		err := ioutil.WriteFile(filePath, []byte(content), fs.ModePerm)
		if err != nil {
			fmt.Printf("failed to write %s: %+v\n", filePath, err)
			os.Exit(1)
		}
	} else {
		b, _ := toolkit.ReadAll(filePath)
		rawStr := string(b)

		template := `

func %sDecorate(c *gin.Context, requestINTF api.Request) (int32, interface{}) {
	return %s(c, requestINTF.(%s))
}`
		importSuffix := `import (
			`
		rawStr = strings.ReplaceAll(rawStr, importSuffix, importSuffix+importReqContent)
		rawStr += fmt.Sprintf(template, actionName, actionName, reqStruct)

		err := toolkit.RewriteFile(filePath, rawStr)
		if err != nil {
			fmt.Printf("controller decorate file: %s add function:  failed! error: "+err.Error(), filePath, actionName)
			return
		}
		fmt.Printf("controller decorate file: %s add function: %s success!\n", filePath, actionName)
	}

	fmt.Printf("generate controller decorate function success! please see the file: %s\n", filePath)
}

func reqOrRespGenerate(t, reqOrRespFileName, actionName string) (requestOrResponsePkgName, reqOrRespStruct, reqOrRespAlias string) {
	var reqOrRespStructName string

	reqOrRespFileNameSlice := strings.Split(reqOrRespFileName, "_")
	for i, v := range reqOrRespFileNameSlice {
		reqOrRespFileNameSlice[i] = strings.ToLower(v)
	}

	reqOrRespSnakeCaseFileName := strings.Join(reqOrRespFileNameSlice, "_")
	var err error
	if t == "request" {
		err, requestOrResponsePkgName, reqOrRespStructName = generate.GenRequest(reqOrRespSnakeCaseFileName, actionName)
	} else if t == "response" {
		err, requestOrResponsePkgName, reqOrRespStructName = generate.GenerateResponse(reqOrRespSnakeCaseFileName, actionName)
	}
	if err != nil {
		fmt.Printf("generate %s failed, %s", t, err.Error())
		os.Exit(1)
	}

	reqOrRespPkgSli := strings.Split(requestOrResponsePkgName, "/")

	reqOrRespStruct = reqOrRespPkgSli[len(reqOrRespPkgSli)-1] + "." + reqOrRespStructName
	if reqOrRespPkgSli[len(reqOrRespPkgSli)-1] != "request" || reqOrRespPkgSli[len(reqOrRespPkgSli)-1] != "response" {
		tmpAliasSli := strings.Split(reqOrRespPkgSli[len(reqOrRespPkgSli)-1], "_")
		for index, tmpAliasV := range tmpAliasSli {
			if index == 0 {
				reqOrRespAlias += toolkit.FirstLower(tmpAliasV)
			} else {
				reqOrRespAlias += toolkit.FirstUpper(tmpAliasV)
			}
		}

		if t == "request" {
			if reqOrRespPkgSli[len(reqOrRespPkgSli)-1] == "request" {
				reqOrRespStruct = "request." + reqOrRespStructName
				reqOrRespAlias = ""
			} else {
				reqOrRespStruct = reqOrRespAlias + "Req." + reqOrRespStructName
				reqOrRespAlias += "Req "
			}
		} else if t == "response" {
			if reqOrRespPkgSli[len(reqOrRespPkgSli)-1] == "response" {
				reqOrRespStruct = "response." + reqOrRespStructName
				reqOrRespAlias = ""
			} else {
				reqOrRespStruct = reqOrRespAlias + "Resp." + reqOrRespStructName
				reqOrRespAlias += "Resp "
			}
		}
	}
	return
}
