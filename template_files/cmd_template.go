package template_files

const CmdTemplate = `package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func init() {
	rootCmd.AddCommand(%sCommand)
}

var (
	%sCommand = &cobra.Command{
		Use:   "%s",
		Short: "%s",
		Long:  "%s-service",
		Run: func(cmd *cobra.Command, args []string) {
			// TODO 初始化各种默认driver
			
			stopChan := make(chan os.Signal)
			signal.Notify(stopChan, syscall.SIGTERM, syscall.SIGINT, syscall.SIGKILL)

			select {
			case signal := <-stopChan:
				fmt.Println("[%s Service] Catch Signal:" + signal.String() + ", and wait 5 sec")
                time.Sleep(5 * time.Second)
			}
		},
	}
)`
