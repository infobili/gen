package template_files

const ControllerTemplate = `package %s

import (
	"gitee.com/infobili/go-drivers/api"
	"github.com/gin-gonic/gin"
	%s%s
)

const ()

var %sModuleCodeToMessageMapping = map[int32]string{}

func init() {
	api.MergeCodeToMessageMapping(%sModuleCodeToMessageMapping)
}

func %s(c *gin.Context%s) (int32, interface{}) {
	// TODO Writing logical code
    
    %s

	return api.Success, %s
}`
