package template_files

const ControllerDecorateTemplate = `// Package %s; Auto generated. DO NOT EDIT.
package %s

import (
	"gitee.com/infobili/go-drivers/api"
	"github.com/gin-gonic/gin"
	%s
)

func %sDecorate(c *gin.Context, requestINTF api.Request) (int32, interface{}) {
	return %s(c, requestINTF.(%s))
}`
