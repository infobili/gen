package template_files

const RequestTemplate = `package %s

import (
	"gitee.com/infobili/go-drivers/api"
	"github.com/gin-gonic/gin"
)

type %s struct {

}

func (req *%s) Validate(c *gin.Context) error {
	if err := c.ShouldBind(&req); err != nil {
		return err
	}

	return nil
}

var _ api.Request = (*%s)(nil)`
