package generate

import (
	"errors"
	"fmt"
	"gitee.com/infobili/gen/template_files"
	"gitee.com/infobili/gen/toolkit"
	"io/fs"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

func GenerateResponse(responseFileName, actionName string) (error, string, string) {
	sep := toolkit.GetDirectorySeparator()
	pkgName := "response"

	if strings.Contains(responseFileName, "/") {
		tmpSlic := strings.Split(responseFileName, "/")
		pkgName = tmpSlic[len(tmpSlic)-2]
	}

	filePath := toolkit.GetRequestOrResponseFilePath(responseFileName, "response")
	responseStructName := actionName + "Response"

	basePath := filepath.Dir(filePath)
	fullPkgName := strings.ReplaceAll(basePath, sep, "/")

	fileExist := toolkit.IsExist(filePath)
	if fileExist {
		template := `

type %s struct {

}`
		b, _ := toolkit.ReadAll(filePath)
		rawStr := string(b)
		rawStr += fmt.Sprintf(template, responseStructName)

		err := toolkit.RewriteFile(filePath, rawStr)
		if err != nil {
			return errors.New(fmt.Sprintf("generate response, file: %s, struct name: %s failed! error: %+v", filePath, responseStructName, err)), "", ""
		}
		fmt.Printf("generate response, file: %s, struct name: %s success!\n", filePath, responseStructName)

		return nil, fullPkgName, responseStructName
	}

	// 文件不存在则生成文件先
	basePathExist := toolkit.IsExist(basePath)
	if !basePathExist {
		// 递归创建文件夹
		err := os.MkdirAll(basePath, os.ModePerm)
		if err != nil {
			return errors.New(fmt.Sprintf("create folder: %s recursively failed! error: "+err.Error(), basePath)), "", ""
		}
	}

	responseStructTemplate := template_files.ResponseTemplate

	content := fmt.Sprintf(responseStructTemplate, pkgName, responseStructName)
	err := ioutil.WriteFile(filePath, []byte(content), fs.ModePerm)
	if err != nil {
		return errors.New(fmt.Sprintf("generate response failed! failed to write %s, struct name: %s, err: %+v\n", filePath, responseStructName, err)), "", ""
	}

	fmt.Printf("generate response, file: %s, struct name: %s success!\n", filePath, responseStructName)

	return nil, fullPkgName, responseStructName
}
