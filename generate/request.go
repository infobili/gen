package generate

import (
	"errors"
	"fmt"
	"gitee.com/infobili/gen/template_files"
	"gitee.com/infobili/gen/toolkit"
	"io/fs"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
)

func GenRequest(requestFileName, actionName string) (error, string, string) {
	sep := toolkit.GetDirectorySeparator()
	pkgName := "request"

	if strings.Contains(requestFileName, "/") {
		tmpSlic := strings.Split(requestFileName, "/")
		pkgName = tmpSlic[len(tmpSlic)-2]
	}

	filePath := toolkit.GetRequestOrResponseFilePath(requestFileName, "request")
	requestStructName := actionName + "Request"

	basePath := filepath.Dir(filePath)
	fullPkgName := strings.ReplaceAll(basePath, sep, "/")

	fileExist := toolkit.IsExist(filePath)
	if fileExist {
		// 文件存在则写入文件
		template := `

type %s struct {

}

func (req *%s) Validate(c *gin.Context) error {
	if err := c.ShouldBind(&req); err != nil {
		return err
	}

	return nil
}

var _ api.Request = (*%s)(nil)`

		b, _ := toolkit.ReadAll(filePath)
		rawStr := string(b)

		// 文件存在且 struct 也存在
		if strings.Contains(rawStr, fmt.Sprintf(" %s ", requestStructName)) {
			fmt.Printf("request is exist, please see: %s, struct name: %s\n", filePath, requestStructName)
			return nil, fullPkgName, requestStructName
		}

		rawStr += fmt.Sprintf(template, requestStructName, requestStructName, requestStructName)
		err := toolkit.RewriteFile(filePath, rawStr)
		if err != nil {
			return errors.New(fmt.Sprintf("generate request, file: %s, struct name: %s failed! error: %+v", filePath, requestStructName, err)), "", ""
		}
		fmt.Printf("generate request, file: %s, struct name: %s success!\n", filePath, requestStructName)

		return nil, fullPkgName, requestStructName
	}

	// 文件不存在则生成文件先
	basePathExist := toolkit.IsExist(basePath)
	if !basePathExist {
		// 递归创建文件夹
		err := os.MkdirAll(basePath, os.ModePerm)
		if err != nil {
			return errors.New(fmt.Sprintf("generate request failed! create folder: %s recursively failed! error: %+v", basePath, err)), "", ""
		}
	}

	content := fmt.Sprintf(template_files.RequestTemplate, pkgName, requestStructName, requestStructName, requestStructName)
	err := ioutil.WriteFile(filePath, []byte(content), fs.ModePerm)
	if err != nil {
		return errors.New(fmt.Sprintf("generate request failed! failed to write %s, struct name: %s, err: %+v\n", filePath, requestStructName, err)), "", ""
	}

	fmt.Printf("generate request, file: %s, struct name: %s success!\n", filePath, requestStructName)

	return nil, fullPkgName, requestStructName
}
